import { LinkedList, Node, Stack } from ".";

export class QueueWithDoubleStacks {
  private stack1: Stack;
  private stack2: Stack;

  constructor(...rest: (string | number)[]) {
    this.stack1 = new Stack();
    this.stack2 = new Stack();
    rest.forEach((item: string | number) => this.enqueue(item));
  }

  private moveFromAToB () {
    while(!this.stack1.isEmpty()) {
      const item = this.stack1.pop();
      if (item) this.stack2.push(item.getVal());
    }
  }

  enqueue(value: number | string) {
    this.stack1.push(value);
  }

  dequeue(): Node | null {
    this.moveFromAToB();
    return this.stack2.pop();
  }

  peek(): Node | null {
    this.moveFromAToB();
    return this.stack2.peek();
  }
}