export class Node {
  public prev: Node | null;
  public next: Node | null;

  constructor(private value: number | string) {
    this.next = this.prev = null;
  }

  setVal(value: number | string) {
    this.value = value;
  }

  getVal(): number | string {
    return this.value;
  }
}

export type FlowType = "stack" | "queue";

export class LinkedList {
  public head: Node | null;
  public tail: Node | null;
  public length: number = 0;

  constructor(...rest: (string | number)[]) {
    this.head = this.tail = null;
    rest.forEach((item: string | number) => this.push(item));
  }

  /**
   * @description pushes a number in the linked list
   * @param value number
   * @returns void
   */
  push(value: number | string) {
    const node = new Node(value);
    this.length++;

    if (this.head === null || this.tail === null) {
      this.head = this.tail = node;
      return;
    }

    this.tail.next = node;
    node.prev = this.tail;
    this.tail = node;
  }

  /**
   * @description deletes the last item in stack (LIFO)
   * @returns Node | null
   */
  pop(): Node | null {
    const popedItem: Node | null = this.tail;

    if (!this.tail || !this.tail.prev) {
      this.head = this.tail = null;
      this.length = 0;
      return popedItem;
    }

    const item = this.tail.prev;
    item.next = null;
    this.tail = item;
    this.length--;
    return popedItem;
  }

  /**
   * @description updates an existing index in the linked list
   * @param index the index of the item to be updated
   * @param value the new value
   * @returns void
   */
  update(index: number, value: number | string) {
    if (index < 0 || index >= this.length || this.head === null) {
      return;
    }

    let item: Node | null = this.head,
      cI: number = 0;

    while (item !== null && cI <= index) {
      if (cI === index) {
        item.setVal(value);
        break;
      }
      cI++;
      item = item.next;
    }
  }

  /**
   * @description check if stack is empty.
   * @returns boolean
   */
  isEmpty(): boolean {
    return !this.head;
  }

  /**
   * @description reverse the link list
   * @returns void
   */
  reverse() {
    if (!this.tail || !this.tail.prev) {
      return;
    }

    let tail: Node | null = null;
    let head: Node | null = null;
    let item: Node | null = this.tail;

    while (item !== null) {
      const node = new Node(item.getVal());

      if (head === null || tail === null) {
        head = tail = node;
        item = item.prev;
        continue;
      }

      tail.next = node;
      node.prev = tail;
      tail = node;
      item = item.prev;
    }

    this.head = head;
    this.tail = tail;
  }

  /**
   * @returns the length of the linked list
   */
  getLength(): number {
    return this.length;
  }

  /**
   * @returns number[] representation of the linked list
   */
  toArray(): (number | string)[] {
    let item = this.head;
    const valueArray: (number | string)[] = [];

    while (item !== null) {
      valueArray.push(item.getVal());
      item = item.next;
    }

    return valueArray;
  }

  hasLoop(): boolean {
    // implement the logic
    return false;
  }

  /**
   * @description logic to middle node of a linked list
   * @returns number[] representation of the linked list item
   * list with odd number of nodes will have 1 middle node
   * otherwise 2
   */
  getMiddleVal(): (number | string)[] {
    let a: Node | null = this.head,
      b: Node | null = this.head;

    // b !== this.tail && b.next !== this.tail
    while (a && b?.next && b?.next !== this.tail) {
      b = b.next.next;
      a = a.next;
    }

    return b?.next && a?.next
      ? [a.getVal(), a.next.getVal()]
      : a
      ? [a.getVal()]
      : [];
  }

  /**
   * @param index the index of the item to be fetched
   * @returns number representation of the linked list item
   */
  getKthVal(index: number): number | string | null {
    if (index < 0 || index >= this.length || this.head === null) {
      return null;
    }

    let item: Node | null = this.head,
      cI: number = 0;
    for (; item && cI < index; cI++) item = item.next;

    return item ? item.getVal() : null;
  }

  /**
   * @description logic is applied for singly linked list
   * @param index the index of the item to be fetched from last
   * @returns number representation of the linked list item
   */
  getKthValFromEndAsSinglyLinkedList(index: number): number | string | null {
    if (index < 0 || index >= this.length || this.head === null) {
      return null;
    }

    let a: Node | null = this.head,
      b: Node | null = this.head,
      cI: number = 0;
    // move forward k items from head
    for (; b && cI < index; cI++) b = b.next;

    // traverse to tail node
    // where b is the node that is k items towards head
    // and a is head
    // when b reaches tail
    // a is k items backwards tail
    while (b !== this.tail && b && a) {
      b = b.next;
      a = a.next;
    }

    return a ? a.getVal() : null;
  }

  /**
   * @description logic applied for doubly linked list
   * @param index the index of the item to be fetched from last
   * @returns number representation of the linked list item
   */
  getKthValFromEndAsDoublyLinkedList(index: number): number | string | null {
    if (index < 0 || index >= this.length || this.tail === null) {
      return null;
    }

    let item: Node | null = this.tail,
      cI: number = this.length - 1;
    for (; item && cI > this.length - 1 - index; cI--) item = item.prev;

    return item ? item.getVal() : null;
  }

  /**
   * @description prints each item present in the linked list
   */
  print() {
    let item = this.head;

    while (item) {
      console.log(item);
      item = item.next;
    }
  }
}
