import { Stack } from ".";

const openingExpressions: string[] = ["(", "{", "[", "<"];
const closingExpressions: string[] = [")", "}", "]", ">"];

export class Expression {
  private stack: Stack = new Stack();
  private inputArray: string[];

  constructor(private input: string) {
    this.inputArray = input.split("");
  }

  private isOpeningBracket(item: string): boolean {
    return openingExpressions.includes(item);
  }

  private isClosingBracket(item: string): boolean {
    return closingExpressions.includes(item);
  }

  private doesBracketsMatch(opening: string, closing: string): boolean {
    return (
      openingExpressions.indexOf(opening) == closingExpressions.indexOf(closing)
    );
  }

  getExpression(): string {
    return this.input;
  }

  isBalanced(): boolean {
    const { stack } = this;

    for (let item of this.inputArray) {
      if (this.isOpeningBracket(item)) stack.push(item);
      else if (this.isClosingBracket(item)) {
        // is there any opening item in the list
        if (stack.isEmpty()) return false;

        // is that the closing bracket
        const popedNode = stack.pop();
        if (!this.doesBracketsMatch(<string>popedNode?.getVal(), <string>item))
          return false;
      }
    }

    return stack.isEmpty();
  }
}
