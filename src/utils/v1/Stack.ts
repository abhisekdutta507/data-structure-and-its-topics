import { LinkedList, Node } from ".";

export class Stack {
  private linkedList: LinkedList;

  constructor(...rest: (string | number)[]) {
    this.linkedList = new LinkedList();
    rest.forEach((item: string | number) => this.push(item));
  }

  /**
   * @description pushes a number in the linked list
   * @param value number
   * @returns void
   */
  push(value: number | string) {
    this.linkedList.push(value);
  }

  /**
   * @description deletes the last item in stack (LIFO)
   * @returns Node | null
   */
  pop(): Node | null {
    return this.linkedList.pop();
  }

  /**
   * @description get the top most item from stack
   * @returns Node | null
   */
  peek(): Node | null {
    return this.linkedList.tail;
  }

  isEmpty(): boolean {
    return this.linkedList.isEmpty();
  }

  toArray(): (string | number)[] {
    return this.linkedList.toArray();
  }
}
