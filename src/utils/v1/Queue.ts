import { LinkedList, Node, Stack } from ".";

export class Queue {
  private linkedList: LinkedList;

  constructor(...rest: (string | number)[]) {
    this.linkedList = new LinkedList();
    rest.forEach((item: string | number) => this.enqueue(item));
  }

  /**
   * @description pushes a number in the linked list
   * @param value number
   * @returns void
   */
  enqueue(value: number | string) {
    const { linkedList } = this;
    const node = new Node(value);
    linkedList.length++;

    if (linkedList.head === null || linkedList.tail === null) {
      linkedList.head = linkedList.tail = node;
      return;
    }

    linkedList.tail.next = node;
    node.prev = linkedList.tail;
    linkedList.tail = node;
  }

  /**
   * @description deletes the 1st item if it is a queue (FIFO)
   * last item if stack (LIFO)
   * @returns void
   */
  dequeue(): Node | null {
    const { linkedList } = this;
    const popedItem: Node | null = linkedList.head;

    if (!linkedList.head || !linkedList.head.next) {
      linkedList.head = linkedList.tail = null;
      linkedList.length = 0;
      return popedItem;
    }

    const item = linkedList.head.next;
    item.prev = null;
    linkedList.head = item;
    linkedList.length--;
    return popedItem;
  }

  /**
   * @description get the top most item from stack
   * @returns Node | null
   */
  peek(): Node | null {
    return this.linkedList.head;
  }

  isEmpty(): boolean {
    return this.linkedList.isEmpty();
  }

  toArray(): (string | number)[] {
    return this.linkedList.toArray();
  }

  /**
   * @description reverse a queue using a stack
   */
  reverse() {
    let { linkedList: { head: item } } = this;
    const stack: Stack = new Stack();

    while(!this.isEmpty()) {
      const removedItem = this.dequeue();
      if (removedItem)
        stack.push(removedItem.getVal());
    }

    while(!stack.isEmpty()) {
      const removedItem = stack.pop();
      if (removedItem)
        this.enqueue(removedItem.getVal());
    }
  }
}
