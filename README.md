### Data Structure in TypeScript

**Setup perdefined** `type` in TypeScript
```ts
type FlowType = 'stack' | 'queue';

const fn = (flow: FlowType) => {
  // syntax
};

class LinkedList {
  constructor(private flow: FlowType) {
    // syntax
  }
}
```

**Command Line Input** in Node.js
```ts
import inquirer from 'inquirer';

const question = {
  type: 'number',
  name: 'choice',
  message: 'Enter your choice:'
};

const response = await inquirer.prompt([question]);
// response.choice
```

Initialize a **Linked List**
```ts
import { LinkedList } from './utils';

const queue = new LinkedList('queue');
const stack = new LinkedList('stack');

// push a number
queue.push(10);

/**
 * @params
 * index - starts from 0
 * updatedValue - new value for that index 
 */
queue.update(1, 6);
// output: []

// pop a number
queue.pop();

// display all the numbers
queue.getVal();
// output: []

// display length of the linked list
queue.getLength();
// output: 0

// print the LinkedList object
queue.print();
```

Logic to find the **Kth node from last** in singly linked list. Runtime complexity **O(n)**
```ts
/**
 * @description logic is applied for singly linked list
 * @param index the index of the item to be fetched from last
 * @returns number representation of the linked list item
 */
getKthValFromEndAsSinglyLinkedList(index: number): number | null {
  if (index < 0 || index >= this.length || this.head === null) {
    return null;
  }

  let a: Node | null = this.head,
    b: Node | null = this.head,
    cI: number = 0;
  // move forward k items from head
  for(; b !== null && cI < index; cI++)
    b = b.next;

  // avoid ts error with b !== null && a !== null
  // otherwise this item is optional
  /**
   * @description traverse to tail node
   * where b is the node that is k items towards head
   * and a is head
   * when b reaches tail
   * a is k items backwards tail
   */
  while(b !== this.tail && b !== null && a !== null) {
    b = b.next;
    a = a.next;
  }

  return a ? a.getVal() : null;
}
```

Find the **middle node** of a linked list. Runtime complexity **O(n)**
```ts
/**
 * @description logic to middle node of a linked list
 * @returns number[] representation of the linked list item
 * list with odd number of nodes will have 1 middle node
 * otherwise 2
 */
getMiddleVal(): number[] {
  let a: Node | null = this.head,
    b: Node | null = this.head;

  // b !== this.tail && b.next !== this.tail
  while(a && b?.next && b?.next !== this.tail) {
    b = b.next.next;
    a = a.next;
  }

  return b?.next && a?.next
    ? [a.getVal(), a.next.getVal()]
    : a
    ? [a.getVal()]
    : [];
}
```

**Floyd’s Cycle-finding Algorithm** to find loop in a linked list:

  - Use two pointers (slow and fast) to traverse the list. Move the slow pointer one step forward and the fast pointer two steps forward. If there’s a loop, at some point, the fast pointer will meet the slow pointer and overtake it.
```ts
// Linked List -> 20- Linked Lists- Exercises
hasLoop(): boolean {
  // implement the logic here
  return false;
}
```

**Real word usage** of **Stacks**:

  - Stacks are used to implement undo & redo features of an Operating System
  - Building compilers for syntax checking. For eg.: ```[(8 - 6) * {(5 + 7) * 12}]```
  - Evaluating expressions. For eg.: ```a+a+++a```
  - Methods available on stacks are `push`, `pop`, `peek`, `isEmpty` & `toArray`

**Real word usage** of **Queues**:

  - Queues are used in `Operating Systems`, `web servers`, `printers`. Basically where we want to process jobs based on the order we have received (FIFO) them.
  - `Printers` use queues to manage printing jobs.
  - `Operating` Syatems use queues to manage processes.
  - `Web servers` use queues to manage incoming requests.
  - Live support systems (Jio customer care. "Please wait. Our executive is busy.")

**Reverse a Queue** using Stack. Runtime complexity **O(n)**
```ts
/**
 * @description Remove the items from Queue & push them in a Stack
 * Pop the items from Stack & enqueue them one after another
 */
reverse() {
  let { linkedList: { head: item } } = this;
  const stack: Stack = new Stack();

  // step 1 - Dequeue items & push them in Stack
  while(!this.isEmpty()) {
    const removedItem = this.dequeue();
    stack.push(removedItem.getVal());
  }

  // step 2 - Pop items & enqueue them in Queue
  while(!stack.isEmpty()) {
    const removedItem = stack.pop();
    this.enqueue(removedItem.getVal());
  }
}
```

**Implement a Queue** using **Stack**. Runtime complexity **O(n)**

  - 2 Stacks are required to implement a Queue
  - [1, 2, 3] in Queue follows FIFO order
  - [1, 2, 3] in Stack will follow LIFO order (1st Stack)
  - [3, 2, 1] in Stack will follow LIFO order (2nd Stack)
  - No pop the items in Stack. The order is 1, 2 & 3. (Achieved the Queue implementation)

```ts
dequeue(): Node | null {
  while(!this.stack1.isEmpty()) {
    const item = this.stack1.pop();
    if (item) this.stack2.push(item.getVal());
  }

  return this.stack2.pop();
}
```

**Peek in QueueWithTwoStacks**. Runtime complexity **O(n)**
```ts
/**
 * @description unlike dequeue peek returns the 1st item in the queue without deleting it.
 */
peek(): Node | null {
  while(!this.stack1.isEmpty()) {
    const item = this.stack1.pop();
    if (item) this.stack2.push(item.getVal());
  }

  return this.stack2.peek();
}
```

**Hash Tables**:

  - Spell checker
  - Dictionaries
  - Compilers
  - Code editors
  - It can lookup for an item superfast
  - For eg.: `Object in JavaScript`, `Dictionary in Python & C#` or `HashMap in Java`
  - Hash tables stores values in a `Key` `Value` pair.
  - Methods available on Hash tables are `insert`, `lookup` & `delete`
```ts
/*
 * @description An example of HashMap data structure
 */
const hashMap = {
  1: 'Audi',
  a: 'BMW',
  null: null
};

console.log(
  hashMap[1],
  hashMap['a'],
  hashMap[null]
);
```

**Insert into Hashtable**. Runtime complexity **O(1)**

```ts
// Coming soon...
```

**Lookup into Hashtable**. Runtime complexity **O(1)**

```ts
// Coming soon...
```

**Delete from a Hashtable**. Runtime complexity **O(1)**

```ts
// Coming soon...
```

**First Non-repeating Character**. Runtime complexity **O(n)**

```ts
const string: string = 'a green apple';
const chatArray: string[] = string.split('');
const hashMap = {};

chatArray.forEach((item) => {
  ob[item] = ob[item] ? ob[item] + 1 : 1;
})

const firstNonRepeatingCharacter = chatArray.find((item) => ob[item] === 1);
```
