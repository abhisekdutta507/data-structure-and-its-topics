FROM node:18.14.0-alpine3.16

WORKDIR /data-structure
COPY package*.json ./
# Cache the node_modules
RUN npm i
COPY ./ ./

CMD ["npm", "run", "start:dev"]